"""
With this script one can make a fit from the tune shift values.
The fit will provide which value you should use as the amplitude of the ripple to have the required tune shift.
"""

import matplotlib.pyplot as plt
from pylab import *

tune_x_no_ripple = 27.27152009
tune_y_no_ripple = 27.32395267

tunes_x = [27.27152009, 27.09820939, 26.87030529, 26.47555977]
tunes_y = [27.32395267, 27.37375543, 27.4395999, 27.55352656]

tune_shift_x = [x-tune_x_no_ripple for x in tunes_x]
tune_shift_y = [y-tune_y_no_ripple for y in tunes_y]

ripples_amplitudes = [0, 0.00025, 0.00058046, 0.0011525]


# linear fit
m, b = polyfit(ripples_amplitudes, tune_shift_x,1)
print m, b

plot(ripples_amplitudes, tune_shift_x, marker="o", label='y = {}*x+({})'.format(m,b))

wanted_tune_shift = 5e-3
required_ripple_amplitude = (wanted_tune_shift-b)/m
print required_ripple_amplitude

plt.xlabel('ripples amplitude')
plt.ylabel('Qx tune_shift')
plt.title('tune shift due in presence of a ripple')
plt.tight_layout()
plt.legend()
plt.show()