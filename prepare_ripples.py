"""
Python 2.7.15

Prepare the files needed for the power supply ripples.
- define and install the new elements (quads, dipoels etc)
- set their strength to zero , so they don't affect the matching
- set their strength a very small value ~e-12 , but slightly different from each other so they all appear in
the fort.2 (fc.2) which will be produced by madx.

"""
import random
import re
import numpy as np



def look_for_elements_return_location(file_name, patterns):
    QFs_location = []  # here is stored the location of the elements in <str> form
    QDs_location = []
    # Find the pattern you want in file and if match append its location in a list
    with open(file_name) as f:
        for line in f:
            for pattern in patterns:
                match = re.findall(pattern, line)
                if match:

                    # print(match[0]
                    at = 'AT\s=\s\d+\\.\d+'
                    location = re.findall(at,line)
                    # keep only the number
                    my_str = location[0]
                    my_str = re.sub('AT\s=\s', '', my_str)  # type
                    if 'QD' in match[0]:
                        QDs_location.append(my_str)
                    elif 'QF' in match[0]:
                        QFs_location.append(my_str)
                    else:
                        print "something went wrong"

        return QFs_location, QDs_location


def definition_and_installation_file(my_QFs_locations, my_QDs_locations):
    f = open("./generated/ripples_quad_definition_and_installation.madx", "w+")

    # first define the multipoles that correspond to the QFs, with (-) strength due to Sixtrack convention
    for i in range(0,len(my_QFs_locations)):
        f.write("k" + str(i) + ":=-dkx1f50l5." + str(i) + ";\ndmqx1f50l5." + str(i) + " : multipole, knl:={0, k"+str(
            i)+"  };\n")
    last_i = i
    print last_i
    # secondly define the multipoles that correspond to the QD, with (+) strength due to Sixtrack convention
    for j in range(1,len(my_QDs_locations)+1):
        f.write("k" + str(i+j) + ":=dkx1f50l5." + str(i+j) + ";\ndmqx1f50l5." + str(i+j) + " : multipole, knl:={0, k"+ str(i+j) + "  };\n")
    f.write("\nuse, sequence=sps;\nseqedit,sequence=sps;\n")

    # Now install the new elements
    # first the QFs
    for i in range(0,len(my_QFs_locations)):
        f.write("install, element=dmqx1f50l5."+str(i)+",at="+str(my_QFs_locations[i])+";\n")
    # secondly the QDs
    for j in range(0,len(my_QDs_locations)):
        f.write("install, element=dmqx1f50l5."+str(i+j)+",at="+str(my_QDs_locations[j])+";\n")
    f.write("endedit;")
    f.close()


def set_strength_to_zero(total_number_of_elements):
    f = open("./generated/ripples_quad_set_strength_zero.madx", "w+")
    for i in range(0,total_number_of_elements):
        f.write("dkx1f50l5."+str(i)+"  := 0;\n")
    f.close()

def set_strength_small_value(total_number_of_elements):
    # create the strengths you will assign to the elements - they must be ~10e-12 (>= 10e-13 is too small)
    strengths = []
    for i in range(0,total_number_of_elements):
        #k = random.random()  # returns a random number [1.0,1.0)
        k = random.uniform(1.0, 10.0)  # returns a random number [1.0,10.0]
        while k in strengths:
            k = random.random()
        strengths.append(k )

    f = open("./generated/ripples_quad_set_different_very_small_strengths.madx", "w+")
    for i in range(0, total_number_of_elements):
        f.write("dkx1f50l5."+str(i)+"  := "+str(strengths[i])+"e-12;\n")
    f.close()


def prepre_DYNK_for_fort_3(total_number_of_elements, frequency, amplitude, phase):
    f = open("./generated/DYNK_for_fort_3", "w+")
    f.write('DYNK\n')
    for i in range(0,total_number_of_elements):
        f.write('FUN RIPP-dmqx1f50l5.{} COSF_RIPP {} {} {}\nSET dmqx1f50l5.{} average_ms RIPP-dmqx1f50l5.{} 1 -1 0\n'.
                format(i, amplitude, frequency, phase, i, i))
    f.write('NEXT\n')
    f.close()


def set_strength_of_kick(total_number_of_elements, amplitude):
    f = open("./generated/ripples_quad_set_strength_kick_for_MADX.madx", "w+")
    for i in range(0,total_number_of_elements):
        f.write("dkx1f50l5."+str(i)+"  := {};\n".format(amplitude))
    f.close()

############ give the parameters ###############################
file_name = './my_sequence/sps_newCCloc.seq' # Give the name of the file you want to look in
flag_run_sixtrack = True
flag_MADX_test = False

# give the pattern you want to look for, as a list of strings
patterns = ['QF\\.\d+']#,'QD\\.\d+'] #diforetika gia to qf kai qd gt einai analga thetiko i arnitiko to strneght pou tha doseis


########## find the locations of the old elements (eg quadrupoles) #############
my_QFs_locations, my_QDs_locations = look_for_elements_return_location(file_name, patterns)
print "QF", len(my_QFs_locations)
print "QD", len(my_QDs_locations)

########## install the new ones, at the same locations ###########
definition_and_installation_file(my_QFs_locations, my_QDs_locations)

total_number_of_elements = len(my_QFs_locations) + len(my_QDs_locations)

if flag_run_sixtrack:
    set_strength_to_zero(total_number_of_elements)
    set_strength_small_value(total_number_of_elements)
    freq = 869  # in turns frev/hz
    tune_shift = 5e-3
    print tune_shift
    beta_x_average = 108.245160841
    amplitude = 4 * np.pi * tune_shift / beta_x_average  # find from tune shift, we consider thin element
    print amplitude
    print amplitude * 1e5
    amplitude_new = '7.72332430084D-6'
    phase = '0.0'
    prepre_DYNK_for_fort_3(total_number_of_elements, freq, amplitude_new, phase)

if flag_MADX_test:
    amplitude = -7.72332430084e-06
    set_strength_of_kick(total_number_of_elements, amplitude)




