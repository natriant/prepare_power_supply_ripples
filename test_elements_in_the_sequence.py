"""
Python 2.7.15

Check the sequence of the machine. In this case we want to check if the QFs and
QDs follow one another or there are two QDs or two QFs in a row.

"""
import re


def look_for_elements(file_name, patterns):
    # Find the pattern you want in file and if match append it a list
    elements_list = []
    with open(file_name) as f:
        for line in f:
            for pattern in patterns:
                match = re.findall(pattern, line)
                if match:
                    elements_list.append(match)
        return elements_list


def first2(s):
    return s[:2]


def qf_qd_in_a_row(elements_list):
    first_two = []
    flag = 'not two in a row'
    for i in range(0,len(elements_list)):
        k = elements_list[i][0]  # str
        first_two.append(first2(k))  # get the first two letters of the string
    for j in range(0,len(first_two)-1):
        if first_two[j] == first_two[j+1]:
            flag = 'two in a row'
    return flag, first_two


file_name = './my_sequence/sps_newCCloc.seq'  # give the name of the file you want to look in
patterns = ['QF\\.\d+', 'QD\\.\d+']  # give the pattern you want to look for, as a list of strings

# find the elements you look for (eg QF, QD)
elements_list = look_for_elements(file_name, patterns)

# run your test
flag_s, first_two_s = qf_qd_in_a_row(elements_list)
print flag_s

# extract the file with the elements in the sequence
extract_file_with_elements = True
if extract_file_with_elements:
    f = open("./generated/elements_in_the_sequence", "w+")
    for i in range(0,len(first_two_s)):
        f.write(first_two_s[i]+"\n")
    f.close()
